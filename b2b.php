<?php

/**
 * @author João Paulo Paes <joaopaulopaez@gmail.com>
 *
 * Retorna de quem é a vez no ping-pong old-school de acordo com o placar informado
 *
 *
 * A partir do placar 20:20 a lógica é a seguinte:
 * O jogador A sempre saca quando a soma do placar divido por 4 resta 0 ou 1
 * Por exemplo:
 *      22:22, a soma é 44 e o resto da divisão por 4 é 12, resta 0, logo é a vez de a.
 *      24:23, a soma é 47 e o resto da divisão por 4 é 11, resta 3, logo é a vez de b.
 *
 *
 * Antes de 20:20
 *
 * O jogador A sempre saca quando a soma do placar divido por 10 e resta um número menor que 5
 * Por exemplo:
 *      6:3, a soma é 9 e o resto da divisão por 10 é 0, resta 9, logo é a vez de b.
 *      7:3, a soma é 10 e o resto da divisão por 10 é 1, resta 0, logo é a vez de a.
 *
 *
 *
 * @param string $placar - Placar no formato "0:0", "21:20", "0:5"
 * @return string
 */
function saque($placar)
{
    $pontos = explode(":", $placar);
    $sum = array_sum($pontos);
    if ($sum >= 40) {
        if ($sum % 4 < 2) {
            return "jogador a";
        } else {
            return "jogador b";
        }
    } else {
        if ($sum % 10 < 5) {
            return "jogador a";
        } else {
            return "jogador b";
        }
    }
}

saque("0:0"); // retorna "jogador a"
saque("3:2"); // retorna "jogador b"
saque("21:20"); // retorna "jogador a"
saque("21:22"); // retorna "jogador b"

saque("21:25"); // retorna "jogador b"
saque("24:25"); // retorna "jogador a"
saque("20:1"); // retorna "jogador a"
saque("1:20"); // retorna "jogador a"
saque("9:22"); // retorna "jogador a"
saque("1:0"); // retorna "jogador a"

saque("6:3"); // retorna "jogador b"
saque("7:3"); // retorna "jogador a"

saque("22:22"); // retorna "jogador a"
saque("24:23"); // retorna "jogador b"